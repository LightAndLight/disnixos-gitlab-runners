with import <nixpkgs/lib>;

let
  validExecutor = e: elem e [
    "shell"
    "docker"
    "docker-ssh"
    "ssh"
    "parallels"
    "virtualbox"
    "docker+machine"
    "docker-ssh+machine"
    "kubernetes"
  ];
in

{ name
, url ? "https://gitlab.com/"
, token
, executor
, tags ? null
, docker ? null
}:
assert (validExecutor executor);
assert ((docker != null) -> (executor == "docker"));

concatStringsSep " \\\n" (

  [
    ''--name "${name}"''
    ''--url "${url}"''
    ''-r "${token}"''
    ''--executor "${executor}"''
  ] ++
  optional (tags != null) (''--tag-list "${concatStringsSep "," tags}"'') ++
  optionals (docker != null) (import ./mkDockerFlags.nix docker)

)
