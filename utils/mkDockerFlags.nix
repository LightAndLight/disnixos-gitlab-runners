with import <nixpkgs/lib>;

{ image ? null
, host ? null
, hostname ? null
}:

map (f: "--docker-" + f) (
  optional (image != null) ''image "${image}"'' ++
  optional (host != null) ''host "${host}"'' ++
  optional (hostname != null) ''hostname "${hostname}"''
)
