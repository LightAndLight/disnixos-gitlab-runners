{ system, pkgs, ... }:

let
  packages = import ./../packages.nix { inherit system pkgs; };
in

rec {
  runner-registration-service = {
    name = "runner-registration-service";
    pkg = packages.runner-registration-service;
    dependsOn = { };
    type = "wrapper";
  };

  gitlab-runner-service = {
    name = "gitlab-runner-service";
    pkg = packages.gitlab-runner-service;
    dependsOn = { inherit runner-registration-service; };
    type = "process";
  };
}
