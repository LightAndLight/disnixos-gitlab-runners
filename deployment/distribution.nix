{ infrastructure }:
{
  runner-registration-service = [ infrastructure.runner ];
  gitlab-runner-service = [ infrastructure.runner ];
}
