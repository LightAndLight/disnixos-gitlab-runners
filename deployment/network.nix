{ 
  runner =
    { config, pkgs, ...}:
    {
      services.disnix.enable = true;

      virtualisation.docker.enable = true;
  
      environment.systemPackages = [ pkgs.gitlab-runner ];
  
      users.extraGroups.gitlab-runner.gid =
        config.ids.gids.gitlab-runner;
  
      users.extraUsers.gitlab-runner = {
        group = "gitlab-runner";
        extraGroups = [ "docker" ];
        uid = config.ids.uids.gitlab-runner;
        createHome = true;
      };
    };
}
