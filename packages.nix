{ system, pkgs }:
let
  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  self = {
    runner-registration-service = callPackage ./packages/runner-registration-service { };
    gitlab-runner-service = callPackage ./packages/gitlab-runner-service { };
  };
in self
