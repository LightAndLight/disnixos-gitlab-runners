{ stdenv
, gitlab-runner
, serviceName ? "gitlab-runner"
, workDir ? "/var/gitlab-runner-service/"
, configPath ? "/var/runner-registration-service/"
, configFileName ? "config.toml"
}:
{ runner-registration-service }:

stdenv.mkDerivation {
  name = "gitlab-runner-service";
  buildCommand =
''
mkdir -p $out/bin
cat > $out/bin/gitlab-runner <<EOF
#! ${stdenv.shell} -e

mkdir -p ${workDir}
${gitlab-runner}/bin/gitlab-runner run \\
  -n "${serviceName}" \\
  -c "${configPath + configFileName}" \\
  -d "${workDir}" \\

EOF

chmod +x $out/bin/gitlab-runner
'';
}
