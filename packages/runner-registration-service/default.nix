{ stdenv
, gitlab-runner
, name ? "runner-registration-service"
, configPath ? "/var/runner-registration-service/"
, configFileName ? "config.toml"
}:

let
  runnerConfig = import ../../runnerConfig.nix;
in

stdenv.mkDerivation {
  inherit name;
  buildCommand =
''
mkdir -p $out/bin
cat > $out/bin/wrapper <<EOF
#! ${stdenv.shell} -e

case "\$1" in
  activate)
    ${gitlab-runner}/bin/gitlab-runner register \\
      -n \\
      -c ${configPath + configFileName} \\
      ${import ../../utils/mkFlags.nix runnerConfig}
      ;;
  deactivate)
    ${gitlab-runner}/bin/gitlab-runner unregister \\
      -c ${configPath + configFileName} \\
      -n ${runnerConfig.name}
      ;;
  esac
EOF

chmod +x $out/bin/wrapper
'';
}
